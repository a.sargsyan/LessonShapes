﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure
{
    class Shape
    {
        public virtual byte Width { get; set; }
        public virtual byte Height { get; set; }

        public virtual void Draw() { }
    }
}